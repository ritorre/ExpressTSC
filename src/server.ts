import * as bodyParser from 'body-parser';
import * as coockieParser from 'cookie-parser';
import * as express from 'express';
import * as logger from 'morgan';
import * as path from 'path';
import errorHandler = require("errorhandler");
import methodOverride = require("method-override");
import mongoose = require('mongoose'); // import mongoose

// Routes
import { IndexRoute } from './routes/index';

// Interfaces
import { IUser } from './interfaces/user';

// Models
import { IModel } from './models/model';
import { IUserModel } from './models/user';

// Schemas
import { userSchema } from './schemas/user';

/**
 * The server
 * @class Server
 */
export class Server {

public app: express.Application;
private model: any; //[IModel] an instance of IModel

/**
 * Bootstrap the application
 * @class Server
 * @method bootstrap
 * @static 
 * @returns {ng.auto.IInjectorService} Returns the newly created injector for this app.
 */
  public static bootstrap(): Server {
    return new Server();
  }

  /**
   * Constructor
   * @class Server
   * @constructor
   */
  constructor() {
    // Instance defaults
    this.model = new Object(); //initialize this to an empty object
    
    // Create expressjs application
    this.app = express();

    // Configure the application
    this.config();

    // Add routes
    this.routes();

    // Add api
    this.api();
  }

  /**
   * Create REST API routes
   * @class Server
   * @method api
   */
  public api () {
    // empty
  }

  /**
   * Configure the application
   * @class Server
   * @method config
   */
  public config () {
    const MONGODB_CONNECTION: string = "mongodb://localhost:27017/heros";    
    this.app.use(express.static(path.join(__dirname, 'public')))
    this.app.set('views', path.join(__dirname, 'views'))
    this.app.set('view engine', 'pug')
    this.app.use(logger('dev'))
    this.app.use(bodyParser.json())
    this.app.use(bodyParser.urlencoded({extended: true}))
    this.app.use(coockieParser('SecretCookieParser'))
    this.app.use(methodOverride());
    global.Promise = require('q').Promise;
    mongoose.Promise = global.Promise;
    let connection: mongoose.Connection = mongoose.createConnection(MONGODB_CONNECTION); // Connect to mongodb
    this.model.user = connection.model<IUserModel>('User', userSchema); // Create models
    this.app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
      err.status = 404;
      next(err);
    });
    this.app.use(errorHandler());
  }

  /**
   * Create router
   * @class Server
   * @method api
   */
  public routes () {
    let router: express.Router;
    router = express.Router();
  
    //IndexRoute
    IndexRoute.create(router);
  
    //use router middleware
    this.app.use(router);
  }
}